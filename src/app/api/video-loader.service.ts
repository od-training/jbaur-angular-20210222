import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { Video, FilterCriteria } from '../types';

@Injectable({
  providedIn: 'root'
})
export class VideoLoaderService {
  filter = new Subject<FilterCriteria>();
  filteredVideos: Observable<Video[]>;

  constructor(private http: HttpClient) {
    const filter = this.filter.pipe(startWith({title: '', author: ''}))
    this.filteredVideos = combineLatest([this.getVideos(), filter]).pipe(
      map(([list, criteria]) => filterVideos(list, criteria))
    )
  }

  getVideos(): Observable<Video[]> {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos').pipe(
      map(videos => uppercaseAuthor(videos))
    );
  }
}

function uppercaseAuthor(list: Video[]): Video[] {
  const newList = list.map(video => {
    const newVideo = { ...video, author: video.author.toUpperCase() }
    return newVideo;
  });
  return newList;
}

function filterVideos(list: Video[], criteria: FilterCriteria): Video[] {
  return list.filter(video => {
    return !criteria.title || video.title.toUpperCase().includes(criteria.title.toUpperCase());
  })
}

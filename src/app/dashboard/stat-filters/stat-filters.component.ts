import { Component, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';

import { VideoLoaderService } from '../../api/video-loader.service';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnDestroy {
  filters: FormGroup;
  filterSub: Subscription;

  constructor(fb: FormBuilder, vls: VideoLoaderService) {
    this.filters = fb.group({
      title: [''],
      author: ['']
    });

    this.filterSub = this.filters.valueChanges.subscribe(
      filterCriteria => vls.filter.next(filterCriteria)
    );
  }

  ngOnDestroy() {
    this.filterSub.unsubscribe();
  }
}

import { Component, OnInit } from '@angular/core';

import { Video } from '../../types';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  currentVideo: Video | undefined;

  constructor() { }

  ngOnInit(): void {
  }

  videoSelected(video: Video) {
    this.currentVideo = video;
  }

}

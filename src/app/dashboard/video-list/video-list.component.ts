import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Video } from '../../types';
import { VideoLoaderService } from '../../api/video-loader.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() selectedVideo: Video | undefined;
  @Output() select = new EventEmitter<Video>();
  videoList: Observable<Video[]>;

  constructor(vls: VideoLoaderService) {
    this.videoList = vls.filteredVideos;
  }

  ngOnInit(): void {
  }

  selectVideo(video: Video) {
    this.select.emit(video);
  }

}
